//VARIABLES GLOBALS
var Apps;
var juindous =new Worker(0, 0, 1, 10, 1);
var becari =new Worker(0, 1, 0.1, 1, 1, 1);
var conBitsCodi = 0;
var espaiTotal = 10;
var espaiOcupat = 0;

var contadorLletres=0;

//funcio que crea una taula que representa l'espai amb un nombre de files n
function creaTaulaEspai (n){
	var taula = $('<table class="taulaEspai"></table>');
	
	for (var i=0;i<n;i++){
		taula.append($('<tr><td id="'+i+'"></td></tr>').hide());
	}
	actualitzaEspai(taula);
	return taula;
}

function actualitzaEspai(t){

	var celes = t.find('td').slice(0,espaiOcupat);
	celes.show();
}

function augmenta (n){
	$('#codiGenerat').html($('#codiGenerat').html()+stringCodiJavascript[contadorLletres]);
	contadorLletres++;
	//$('pre code').each(function(i, e) {hljs.highlightBlock(e)});
	
	
	Linies.num=Linies.num+n;
	//creaBit();
	
}
function creaBit(){
	var margin=(Math.random()*new Date().getTime())%200;
	var bit= Math.round((Math.random()*10))%2===0?'1':'0';
	var idBit = 'bitBoto'+new Date().getTime();
	conBitsCodi++;
	creaBitCode(100, margin, bit, idBit);
	setTimeout(creaBitCode, 100, 50, margin, bit, idBit);
	setTimeout(creaBitCode, 200, 25, margin, bit, idBit);
	
	setTimeout(function(){
		$('.bbc'+idBit).remove();
		conBitsCodi--;}	,500);
	
}
function creaBitCode( opacitat, margin, bit, idBit){	
	var html = '<div class="bitBotoCode bbc'+idBit+'" style="opacity:'+opacitat/100+'; margin-left: '+margin+'px; margin-bottom: '+new Date().getTime()*3%50+'px;">'+bit+'</div>';
	//bitBotoCode
	$('#bitContainer').append( html );	
}

function compraWorker(t){
	if (Diners.num<t.preu) {
		alert ('NO diniero AMIGO');
		return;
	}
	if ((t.espai + espaiOcupat) > espaiTotal) {
		alert('NO hay espacio');
		return;
	}
	//Restem les linies que generen tots els workers d'aquest tipus
	Linies.num-=t.lxs*t.num;
	//Idem pels diners (pero a la inversa)
	Diners.num+=t.dxs*t.num;
	//Augmentem el numero de workers d'aquest tipus
	t.num++;
	//Tornem a calcular les linies i diners que generen 
	Linies.num+=t.lxs*t.num;
	Diners.num-=t.dxs*t.num;
	//Recalculem el preu
	t.preu = t.preu+(t.curva*t.num);

}

function compraApp(t){
alert(Apps[t].nom);

	if (Linies.num<Apps[t].preu) {
		alert ('NO Linies AMIGO');
		return;
	}
	//Augmentem el numero de apps d'aquest tipus
	Apps[t].num++;
	//Restem els diners que generen totes les aplicacions d'aquest tipus
	//Diners.num-=Apps[t].dxs*Apps[t].num;
	//console.log('Diners num: ' + Diners.num);
	//Idem per les linies (pero a la inversa)
	//Linies.num+=Apps[t].lxs*Apps[t].num;
	//console.log('Linies num: ' + Linies.num);
	//console.log('Apps['+ t +'] num: ' + Apps[t].num);
	//Tornem a calcular les linies i diners que generen 
	//Linies.num = Linies.num - (Apps[t].lxs*Apps[t].num);
	//Diners.num = Diners.num + (Apps[t].dxs*Apps[t].num);
	//console.log('APPS lxs:' + Apps[t].lxs);
	//console.log('APPS dxs:' + Apps[t].dxs);
	//Recalculem el preu


	//Restar preu en linies de les linies totals

	Linies.num = Linies.num - Apps[t].preu;
	//Canviar el dxs de diners
	Diners.dxsPos = Diners.dxsPos + Apps[t].dxs;

	Apps[t].preu = Apps[t].preu+(Apps[t].curva*Apps[t].num);
	$('#casellaApp'+t).html(Apps[t].preu + '&lt/&gt; '+ Apps[t].dxs +'dxs');

}
    
function actualitzador() {

      $('#liniesTotals').html('Linies totals: '+Math.round(Linies.num,2));
      console.log('Linies totals WW: '+Math.round(Linies.num,2));
	  $('#liniesLxs').html('LXS: '+Linies.lxs());
	  $('#dinersTotals').html('Diners totals: '+Math.round(Diners.num,2));
	  $('#dinersLxs').html('DXS: '+Diners.dxs());
	  $('#espai').html('Espai: ' + espaiOcupat + '/'+ espaiTotal);
	  //console.log('|');
    setTimeout(actualitzador , 10);
    }
	
function calculIndex(){
	Linies.num=Linies.num+Linies.lxsPos*0.1;
	Linies.num=Linies.num-Linies.lxsNeg*0.1;
	Diners.num=Diners.num+Diners.dxsPos*0.1;
	Diners.num=Diners.num-Diners.dxsNeg*0.1;
	if (Diners.num<=0)Diners.num=0;
	if (Linies.num<=0)Linies.num=0;
	//console.log('<>');
	setTimeout(calculIndex , 100);
}
function ompleContainerApps(){
	
	var table =  $('<table></table>');
	var x=0;
	for(var i=1; i<=4; i++){
		var row = $('<tr></tr>');
			for(var k=1;k<=4;k++){
			
				var cell = $('<td data-toggle="popover" onClick="compraApp('+x+')" data-container="body" data-trigger="hover" title="App' + '-' + k + '" data-content="Descripcio App' + '-' +k + '" ></td>');
				cell.append(Apps[x].nom + '<br/><div id="casellaApp'+x+'"  style="font-size:10px;">' + Apps[x].preu + '&lt/&gt; '+ Apps[x].dxs +'dxs</div>');
				//cell.css("background", "url("+Apps[x].img+")");
				//cell.css("background-size", "cover");
				row.append(cell);
				x++;
			}
		table.append(row);
	}
	$('#containerApps').append(table);

}
function ompleContainerWorkers(){
	
	var table =  $('<table></table>');
	var x=0;
	for(var i=1; i<=4; i++){
		var row = $('<tr></tr>');
			for(var k=1;k<=4;k++){
			x++;
				var cell = $('<td data-toggle="popover" data-container="body" data-trigger="hover" title="Worker' + '-' + k + '" data-content="Descripcio Worker' + '-' +k + '" ></td>');
				cell.append('Worker'+x);
				row.append(cell);
			}
		table.append(row);
	}
	$('#containerWorkers').append(table);
}

function ompleContainerDecisions(){
	
	var table =  $('<table></table>');
	var x=0;
	for(var i=1; i<=4; i++){
		var row = $('<tr></tr>');
			for(var k=1;k<=4;k++){
			x++;
				var cell = $('<td></td>');
				cell.append('Decisio'+x);
				row.append(cell);
			}
		table.append(row);
	}
	$('#containerDecisions').append(table);
}

function containerEdificis(){
	
	var table =  $('<table></table>');
	var x=0;
	for(var i=1; i<=4; i++){
		var row = $('<tr></tr>');
		x++;
		var cell = $('<td></td>');
		cell.append('Edifici'+x);
		row.append(cell);
		table.append(row);
	}
	$('#containerEdificis').append(table);

}
function seleccionaContainerEsq(entrada){
	$('#containerEdificis').hide();
	$('#containerDecisions').hide();
	$('#containerWorkers').hide();
	$('#containerApps').hide();
	$('#container'+entrada).show();
}

function core() {
	
	var Apps= [App];
	//Inicialitzem menu esquerre
	ompleContainerApps();
	ompleContainerWorkers();
	containerEdificis();
	ompleContainerDecisions();
	seleccionaContainerEsq('Apps');
	//Inicialitzar popovers boostrap
	/*$(function () {
		$('[data-toggle="popover"]').popover({
			placement : 'right'
		})
	});*/
	//Inicialitzador fils
    setTimeout(actualitzador() , 500);
	setTimeout(calculIndex() , 1000);
	
}