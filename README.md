Introducció

Funcionament principal del joc. Defineix que:

* L'usuari mirara d'aconseguir el major nombre possible de diners i/o linies de codi
* L'usuari interactuarà amb els sistemes utilitzables del joc per aconseguir-ho
* Interactuant amb la vista
* Apretant el botó de generacio de codi
* Utilitzant el sistema de compra
* Comprant elements del cicle de treball
* Millorant aquests elements
* Utilitzant el sistema de decisions
* Aconseguint bonificacions volatils
* Modificant els elements del clima
* El cicle de treball es veura afectat pels sistemes automatics del joc i el clima
* Fil de la vida
* Fil que manté el control sobre el com es desenvolupa el joc
* Actualitza valors de pantalla
* Porta un sistema d'events
* Una serie de comprovacions al llarg del temps
* Una serie de comprovacions donats certs escenaris
* Clima
* L'espai limitara la quanitat de treballadors que podem contractar
* El Karma determinara la alineació del jugador
* La alineació afectara a com es desenvolupa el joc